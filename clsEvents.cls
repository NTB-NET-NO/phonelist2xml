VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
  Persistable = 0  'NotPersistable
  DataBindingBehavior = 0  'vbNone
  DataSourceBehavior  = 0  'vbNone
  MTSTransactionMode  = 0  'NotAnMTSObject
END
Attribute VB_Name = "clsEvent"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = True
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Private mvarTime As String
Private mvarFolderName As String
Private mvarSubfolders As Integer
Private mvarDelete As Integer
Private mvarLastRun As Date

Public Property Get Time() As String
    Time = mvarTime
End Property

Public Property Let Time(ByVal NewValue As String)
    Dim timeNow As Date
    Dim limDate As Date
    
    mvarTime = Replace(NewValue, ".", ":")
    
    'Sette lastRun til 24 timer f�r hendelsen skal kj�res
    timeNow = Now()
    limDate = FormatDateTime(timeNow, vbShortDate) & " " & mvarTime
    If DateDiff("n", timeNow, limDate) > 0 Then
        limDate = FormatDateTime(DateSerial(Year(timeNow), Month(timeNow), Day(timeNow) - 1), vbShortDate) & " " & mvarTime
    End If
    
    mvarLastRun = limDate
End Property

Public Property Get FolderName() As String
    FolderName = mvarFolderName
End Property

Public Property Let FolderName(ByVal NewValue As String)
    mvarFolderName = NewValue
End Property

Public Property Get Subfolders() As Integer
    Subfolders = mvarSubfolders
End Property

Public Property Let Subfolders(ByVal NewValue As Integer)
    mvarSubfolders = NewValue
End Property

Public Property Get Delete() As Integer
    Delete = mvarDelete
End Property

Public Property Let Delete(ByVal NewValue As Integer)
    mvarDelete = NewValue
End Property

Public Property Get LastRun() As Date
    LastRun = mvarLastRun
End Property

Public Property Let LastRun(ByVal NewValue As Date)
    mvarLastRun = NewValue
End Property

