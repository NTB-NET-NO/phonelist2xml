Attribute VB_Name = "ModINIFileHandling"

Option Explicit

'Library functions from kernel32
Public Declare Function gfnGetPrivateProfileString Lib "KERNEL32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As Any, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Long, ByVal lpFileName As String) As Long
Public Declare Function gfnGetPrivateProfileInt Lib "KERNEL32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal nDefault As Long, ByVal lpFileName As String, ByVal nSize As Long, ByVal lpFileName As String) As Long

Function gfnGetFromIni(iniFile As String, strSection As String, strKey As String, strDefaultValue As String) As String

    Dim intI As Long
    Dim strReturnString As String   'String buffer
    
    'Fills string buffer with null-character (Ascii char number 0)
    strReturnString = String$(150, Chr$(0))

    'Gets value from ini file
    gfnGetPrivateProfileString strSection, strKey, strDefaultValue, strReturnString, Len(strReturnString) + 1, iniFile
    
    'Removes null-characters from end of string
    For intI = 0 To Len(strReturnString) + 1
        If Asc(Right(Left(strReturnString, intI + 1), 1)) = 0 Then
            Exit For
        End If
    Next

    'Returns String without null characters
    gfnGetFromIni = Left(strReturnString, intI)

End Function
