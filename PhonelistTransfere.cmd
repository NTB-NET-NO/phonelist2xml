@echo off

rem Set paths:
rem --------------------------------------------
set BatchPath=C:\fip\local\scripts\PortalPhonelist
set TempPath=C:\fip\local\scripts\PortalPhonelist\Temp

rem Set current directory to Phonelist folder
rem --------------------------------------------
C:
cd %TempPath%

rem Main program:
rem --------------------------------------------

date /T >%BatchPath%\PhonelistTransfere.out
time /T >>%BatchPath%\PhonelistTransfere.out
echo --------------------------------->>%BatchPath%\PhonelistTransfere.out
echo Send Phonelist to portal >>%BatchPath%\PhonelistTransfere.out

if errorlevel 1 goto error

ftp -s:%BatchPath%\PhonelistTransfere.ftp 193.75.33.39 >>%BatchPath%\PhonelistTransfere.out

rem Check if transfere was OK
find "226 Transfer complete." %BatchPath%\PhonelistTransfere.out>nul

if errorlevel 1 goto error
	echo --------------------------------->>%BatchPath%\PhonelistTransfere.out
	echo Deleting files:>>%BatchPath%\PhonelistTransfere.out

	del %TempPath%\*.xml /Q>>%BatchPath%\PhonelistTransfere.out

	echo ------------->>%BatchPath%\PhonelistTransfere.out
	echo Transfere OK!>>%BatchPath%\PhonelistTransfere.out

goto end

:error
	echo ----------------->>%BatchPath%\PhonelistTransfere.out
	echo Transfere FAILED!>>%BatchPath%\PhonelistTransfere.out

:end
