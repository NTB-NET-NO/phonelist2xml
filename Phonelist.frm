VERSION 5.00
Begin VB.Form Phonelist 
   Caption         =   "Phonelist2XML"
   ClientHeight    =   3195
   ClientLeft      =   60
   ClientTop       =   345
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   3195
   ScaleWidth      =   4680
   StartUpPosition =   3  'Windows Default
   Visible         =   0   'False
End
Attribute VB_Name = "Phonelist"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'Date:          april 2002
'Author:        Terje Kristiansen

Option Explicit

'Implements ICcrpTimerNotify
'Dim Timer As ccrpTimer

Dim mpsSession As MAPI.Session
Dim InfoStore As MAPI.InfoStore
Dim isInfoStore As MAPI.InfoStore
Dim objInfoStore

Dim lngError As Long
Dim strError As String

'Variables to hold INI-values
'These values are assigned in GetIniValues() in this code
Dim strInfoStoreName As String  'InfoStore=
Dim strProfileName As String     'Server=
Dim nRetryInterval As Long      'RetryInterval=
Dim nInterval As Long           'RetryInterval=
Dim strSchedFile As String      'SchedFile=
Dim LOGFILE As String           'LogFile=
Dim strOutDirFile As String     'Output XML file
Dim strLine As String           'Variable holding the Folder

'Logger object
Dim Logger As cLogger

'Collection of entries
Dim colEvents As Collection

'root folder of infostore
Dim fRoot As MAPI.Folder
Dim fError As MAPI.Folder

Private Sub Form_Load()

    On Error GoTo Err_Load

    Dim strDisplayName As String
    Dim bStarted As Boolean
    Dim iIniErr As Integer
    Dim varFolderName As Variant
    Dim iniFile As String
    Dim thisEvent As clsEvent
            
    Set Logger = New cLogger
    
    'Set Timer = New ccrpTimer
    'Set Timer.Notify = Me
    
    iniFile = "c:\Phonelist2XML\Phonelist2XML.ini"
    'iniFile = "D:\utvikling\Phonelist2XML.ini"
    'iniFile = "d:\projects\phonelist2xml\Phonelist2XML.ini"
    
    If LCase(Left(Command, 2) = "-i") Then
        iniFile = Trim(Right(Command, Len(Command) - 2))
    
    ElseIf Command <> "" Then
        MsgBox "Invalid command option"
        End
    End If
    
    'Read ini file values
    iIniErr = GetIniValues(iniFile)
    Logger.Log LOGFILE, "Load !s", "Settings read from " & iniFile
    
    'Read config file
    iIniErr = ReadSchedFile(strSchedFile)
    Logger.Log LOGFILE, "Load !s", "Schedule read from " & strSchedFile
       
    'This service will be run whenever the timer-event occurs so start the timer
    'Timer.Interval = 1      'set interval
    'Timer.Enabled = True    'enable timer
    
    'From ICcrpTimerNotify_Timer
    'Log in if we're not there yet
    Do While fRoot Is Nothing
        GetSession
    Loop

    For Each thisEvent In colEvents
        Logger.Log LOGFILE, "Timer !t", "Difference for " & thisEvent.FolderName
        Phonelist2XMLFolder thisEvent
    Next
       
    
    End
    'Exit Sub
    
Err_Load:
    Logger.Log LOGFILE, "Load !x", Err.Description
    End
    
End Sub

'Private Sub ICcrpTimerNotify_Timer(ByVal Milliseconds As Long)
'    'Event handler for timer control
'    On Error GoTo Err_Timer
'
'    Dim thisEvent As clsEvent
'
'    'Disable timer while processing
'    Timer.Enabled = False
'
'    'Log in if we're not there yet
'    Do While fRoot Is Nothing
'        GetSession
'    Loop
'
'    'Loop through the events
'    For Each thisEvent In colEvents
'        Logger.Log LOGFILE, "Timer !t", "Difference for " & thisEvent.FolderName
'        Phonelist2XMLFolder thisEvent
'    Next
'
'    'Set real interval
'    Timer.Interval = nInterval
'    Timer.Enabled = True
'
'    Exit Sub
'
'Err_Timer:
'    Logger.Log LOGFILE, "Timer !x", Err.Description
'    Timer.Enabled = True
'
'End Sub

Private Sub Phonelist2XMLFolder(thisEvent As clsEvent)
    
    Dim objFolder As MAPI.Folder
    Dim objNewFolder As MAPI.Folder
    Dim objTmpFolder As MAPI.Folder
'    Dim objSubFolders As MAPI.Folders
    
    Dim objFields 'As MAPI.Fields
    Dim strName 'As MAPI.Field
    Dim strEpost 'As MAPI.Field
    Dim strAvdeling As String
    Dim strStilling As String
    Dim strMSN As String
    Dim strPhone As String
    Dim strMobil As String
    Dim strSign As String
    Dim strHomePhone As String
    Dim strStreet As String
    Dim strZip As String
    Dim strCity As String
    Dim strXMLData As String
    Dim filehandle
    
    Dim tmpBirth As String
    Dim strBirth As String
        
    Dim strNow
    Dim strXML
    
    Dim objMessage As Object

    Dim timeNow As Date
    Dim msgDate As Date
    
    Dim strMsgYear As String
    Dim strMsgMonth As String
    
    Dim i As Integer
    Dim intMessages As Integer
    
    On Error GoTo Phonelist2XML_err
    
    Logger.Log LOGFILE, "Phonelist2XMLFolder !i", "START Skjekk Folder " & thisEvent.FolderName
    
    'Get actual folder
    Set objFolder = GetFolderByName(thisEvent.FolderName, fRoot.Folders)
    Logger.Log LOGFILE, "Phonelist2XMLFolder !i", objFolder.Messages.Count & " Contacts/messages"
    
    timeNow = Now()
    
    'Get collection size
    intMessages = objFolder.Messages.Count

    filehandle = FreeFile

    Open strOutDirFile For Output As #filehandle 'Open file for output

    strXML = "<?xml version=""1.0"" encoding=""ISO-8859-1"" standalone=""yes""?>"
    strNow = Format(Now(), "dd.MM.yyyy HH:mm:ss")
    
    Print #filehandle, strXML
    Print #filehandle, "<telefonliste>"
    Print #filehandle, "<timestamp>" & strNow & "</timestamp>"
        
    On Error Resume Next
        
    For i = 1 To intMessages
        Set objMessage = objFolder.Messages.Item(i)

        Set objFields = objMessage.Fields
        
        'Nullstiller felter
        strName = ""
        strEpost = ""
        strMSN = ""
        strBirth = ""
        tmpBirth = ""
        strAvdeling = ""
        strStilling = ""
        strPhone = ""
        strMobil = ""
        strHomePhone = ""
        strSign = ""
        strStreet = ""
        strZip = ""
        strCity = ""
        
        objMessage.Fields(CdoPR_MESSAGE_CLASS).Value = "IPM.Contact.NTB-medarbeidere"
       ' objMessage.Update
        
        'Etternavn, Fornavn
        strName = objFields.Item("{0420060000000000C000000000000046}0x8005")
            strName = Replace(strName, "&", "&amp;")
            
        'E-post addresse
        'Set strEpost = objFields.Item("{0420060000000000C000000000000046}0x8084")
        '    strEpost = Replace(strEpost, "&", "&amp;")
        
        'E-post address - egendefinertFelt
        strEpost = Trim(objMessage.Fields("EpostAddresse").Value)
            If strEpost = "" Then
                'strEpost = objFields.Item("{0420060000000000C000000000000046}0x8084")
            End If
        strEpost = Replace(strEpost, "&", "&amp;")
            
        strSign = Trim(objMessage.Fields("Signatur").Value)
            strSign = Replace(strSign, "&", "&amp;")
        
        'Henter avdelingsnavnet
        strAvdeling = objMessage.Fields(CdoPR_COMPANY_NAME).Value
            strAvdeling = Replace(strAvdeling, "&", "&amp;")
            
        'Henter stilling
        strStilling = Trim(objMessage.Fields(CdoPR_TITLE).Value)
        strStilling = UCase(Left(strStilling, 1)) & Mid(strStilling, 2)
        strStilling = Replace(strStilling, "&", "&amp;")
            
        'Henter internt Telefonnummer
        strPhone = Trim(objMessage.Fields(CdoPR_BUSINESS_TELEPHONE_NUMBER).Value)
        
        ' Henter mobilnummeret
        strMobil = Trim(objMessage.Fields(CdoPR_MOBILE_TELEPHONE_NUMBER).Value)
        
        ' Henter hjemmenummeret - Ny LCH
        strHomePhone = Trim(objMessage.Fields(CdoPR_HOME_TELEPHONE_NUMBER).Value)
        
        ' Henter MSN logon - Ny LCH
        strMSN = Trim(objMessage.Fields("msnlogon").Value)
        
        ' Henter f�dselsdag - Ny LCH
        tmpBirth = objMessage.Fields("strBirthdate").Value
        If tmpBirth <> "" And tmpBirth <> "Ingen" Then
           strBirth = Right(tmpBirth, 4) & "-" & Mid(tmpBirth, 4, 2) & "-" & Left(tmpBirth, 2)
       End If
        
        ' Henter Gate - Ny LCH
        strStreet = Trim(objMessage.Fields(CdoPR_HOME_ADDRESS_STREET).Value)
        
        ' Henter Zip - Ny LCH
        strZip = Trim(objMessage.Fields(CdoPR_HOME_ADDRESS_POSTAL_CODE).Value)
        
        ' Henter By - Ny LCH
        strCity = Trim(objMessage.Fields(CdoPR_HOME_ADDRESS_CITY).Value)
               
        strXMLData = "<person><navn>" & strName & "</navn><avdeling>" & strAvdeling & "</avdeling><telefon>" & strPhone & "</telefon>"
        
        If strSign <> "" Then strXMLData = strXMLData & "<signatur>" & strSign & "</signatur>"
        If strStilling <> "" Then strXMLData = strXMLData & "<stilling>" & strStilling & "</stilling>"
        If strMSN <> "" Then strXMLData = strXMLData & "<messenger>" & strMSN & "</messenger>"
        If strBirth <> "" Then strXMLData = strXMLData & "<birthdate>" & strBirth & "</birthdate>"
        If strEpost <> "" Then strXMLData = strXMLData & "<epost>" & strEpost & "</epost>"
        If strMobil <> "" Then strXMLData = strXMLData & "<mobil>" & strMobil & "</mobil>"
        If strHomePhone <> "" Then strXMLData = strXMLData & "<privat>" & strHomePhone & "</privat>"
        If strStreet <> "" Then strXMLData = strXMLData & "<adresse_gate>" & strStreet & "</adresse_gate>"
        If strZip <> "" Then strXMLData = strXMLData & "<adresse_postnr>" & strZip & "</adresse_postnr>"
        If strCity <> "" Then strXMLData = strXMLData & "<adresse_by>" & strCity & "</adresse_by>"
        
        strXMLData = strXMLData & "</person>"
        
        Print #filehandle, strXMLData

     '   Logger.Log LOGFILE, "Fra Phonelist2XMLFolder - !i", "Subject : " & objMessage.Subject
     '   Logger.Log LOGFILE, "Fra Phonelist2XMLFolder - !i", "DislayName : " & objMessage.Fields(CdoPR_DISPLAY_NAME).Value
     '   Logger.Log LOGFILE, "Fra Phonelist2XMLFolder - !i", "Avdeling : " & objMessage.Fields(CdoPR_COMPANY_NAME).Value
     '   Logger.Log LOGFILE, "Fra Phonelist2XMLFolder - !i", "Telefonnummer : " & objMessage.Fields(CdoPR_BUSINESS_TELEPHONE_NUMBER).Value
     '   Logger.Log LOGFILE, "Fra Phonelist2XMLFolder - !i", "Initialer : " & objMessage.Fields(CdoPR_INITIALS).Value
     '   Logger.Log LOGFILE, "Fra Phonelist2XMLFolder - !i", "MobilePhone : " & objMessage.Fields(CdoPR_MOBILE_TELEPHONE_NUMBER).Value
     '   Logger.Log LOGFILE, "Fra Phonelist2XMLFolder - !i", "Initialer : " & objMessage.Fields(CdoPR_INITIALS).Value
        
     '   Logger.Log LOGFILE, " ", "Navn : " & strName
     '   Logger.Log LOGFILE, " ", "Avdeling : " & objMessage.Fields(CdoPR_COMPANY_NAME).Value
     '   Logger.Log LOGFILE, " ", "Telefonnummer : " & objMessage.Fields(CdoPR_BUSINESS_TELEPHONE_NUMBER).Value
     '   Logger.Log LOGFILE, " ", "MobilePhone : " & objMessage.Fields(CdoPR_MOBILE_TELEPHONE_NUMBER).Value
     '   Logger.Log LOGFILE, " ", "E-mail : " & strEpost
    Next
    
    Set objFolder = Nothing
    
    thisEvent.LastRun = FormatDateTime(Now(), vbShortDate) & " " & thisEvent.Time
    
    Print #filehandle, "</telefonliste>"
    Close #filehandle
    
    Exit Sub
    
Phonelist2XML_err:
    Logger.Log LOGFILE, "Error Phonelist2XMLFolder !x", Err.Description
    Err.Clear
    Resume Next
End Sub

Private Function ReadSchedFile(strSchedFile) As Integer
    Dim fh As Integer
    Dim i As Integer
    Dim strKeyword
    Dim strDefTime As String
    Dim entries As Variant
    Dim schedEvent As clsEvent
    
    On Error GoTo readsched_err
    
    Set colEvents = New Collection

            strDefTime = "00:00"

            Set schedEvent = New clsEvent
            With schedEvent
                .Time = strDefTime
                .FolderName = strLine

                colEvents.Add schedEvent
            End With
            
    Set schedEvent = Nothing
    Exit Function
    
readsched_err:
    Logger.Log LOGFILE, "ReadSchedFile !x", Err.Description
    ReadSchedFile = -1
End Function

Private Function GetIniValues(iniFile As String) As Integer

    On Error GoTo GetIniError
    
    Dim strtemp As Variant
    
'***[WinNT]
    
    'get logfile location. NB! Read this value first from ini file, or nothing will be logged
    LOGFILE = gfnGetFromIni(iniFile, "WinNT", "LogFile", "")
    
    On Error Resume Next
    'Sletter f�rst loggfile
    Kill LOGFILE
    Logger.Log LOGFILE, "GetIniValues !s", "-- Logging started --"
    
    On Error GoTo GetIniError
    
    'get sched file
    strSchedFile = gfnGetFromIni(iniFile, "WinNT", "SchedFile", "")
    Logger.Log LOGFILE, "GetIniValues !s", "SchedFile=" & strSchedFile
    
    strLine = gfnGetFromIni(iniFile, "Folder", "Folder", "")
    Logger.Log LOGFILE, "ReadSchedFile !i", "strLine = " & strLine

    'get poll interval
    nInterval = CLng(Val(gfnGetFromIni(iniFile, "WinNT", "Interval", "1")) * 1000)
    Logger.Log LOGFILE, "GetIniValues !s", "Interval=" & nInterval
    
    'get output folder and file
    strOutDirFile = gfnGetFromIni(iniFile, "WinNT", "OutPutDirFile", "")

'***[Exchange]
    'get profile name
    strProfileName = gfnGetFromIni(iniFile, "Exchange", "Profile", "")
    Logger.Log LOGFILE, "GetIniValues !s", "Profile = " & strProfileName

    
    'get infostore name from section "Exchange" of ini file, default value = "Public folders"
    strInfoStoreName = gfnGetFromIni(iniFile, "Exchange", "InfoStore", "")
    Logger.Log LOGFILE, "GetIniValues !s", "InfoStore=" & strInfoStoreName
               
    'get logon retry interval
    nRetryInterval = CLng(Val(gfnGetFromIni(iniFile, "Exchange", "RetryInterval", "10") * 1000))
    Logger.Log LOGFILE, "GetIniValues !s", "RetryInterval=" & nRetryInterval
    
    Exit Function
    
GetIniError:
    Logger.Log LOGFILE, "GetIniValues !x", Err.Number & " " & Err.Description
    
End Function


Private Sub GetSession()

    Dim strPublicRootID As String

    Dim tagPublicRootEntryID As Long

    'Dim strProfile As String

 

    Err.Clear

    

    On Error Resume Next

    

  

'Create new session

Set mpsSession = New MAPI.Session


Const PR_IPM_PUBLIC_FOLDERS_ENTRYID = &H66310102
Dim rdmutils
Set rdmutils = CreateObject("Redemption.MAPIUtils")

Dim oSession As RDOSession
Dim objInfoStore 'As RDOStore

 
Set oSession = CreateObject("Redemption.RDOSession")
    'dersom det er angitt et profilnavn i ini fila, bruk den, ellers logg p� den gjeldende session
    If strProfileName = "" Then
        'logg p� med gjeldende profil
        mpsSession.Logon , , False, False, 0, True
        oSession.Logon , , False, False, 0, True
    Else
        'logg p� angitt profil
        mpsSession.Logon ProfileName:=strProfileName, ShowDialog:=False, NewSession:=True, ParentWindow:=0, NoMail:=True
        oSession.Logon strProfileName, , False, True, 0, True
    End If


    Set objInfoStore = oSession.Stores.FindExchangePublicFoldersStore
    'Get InfoStore and root folder
    'Set isInfoStore = Nothing
    Set fRoot = Nothing
    Logger.Log LOGFILE, "GetSession !s", "Getting InfoStore and RootFolder"

    'Open Public Folders (no 1) if no InfoStore is specified
    If strInfoStoreName = "" Then
        Set isInfoStore = mpsSession.InfoStores(2)
        'strPublicRootID = isInfoStore.Fields(tagPublicRootEntryID).Value
        strPublicRootID = rdmutils.HrArrayToString(objInfoStore.Fields(PR_IPM_PUBLIC_FOLDERS_ENTRYID))
        Set fRoot = mpsSession.GetFolder(strPublicRootID, isInfoStore.ID)
    Else
        Set isInfoStore = GetInfoStoreByName(mpsSession, strInfoStoreName)
        'strPublicRootID = isInfoStore.Fields(tagPublicRootEntryID).Value
        strPublicRootID = rdmutils.HrArrayToString(objInfoStore.Fields(PR_IPM_PUBLIC_FOLDERS_ENTRYID))
        Set fRoot = mpsSession.GetFolder(strPublicRootID, isInfoStore.ID)
    End If
    
    
    
    
    

    Set objInfoStore = Nothing
    Set rdmutils = Nothing
    Set oSession = Nothing
    'Check errors

    If Err.Number <> 0 Then
        Logger.Log LOGFILE, "GetSession !x", Err.Number & " " & Err.Description
        mpsSession.Logoff
        Set mpsSession = Nothing
        Sleep nRetryInterval
    Else
        Logger.Log LOGFILE, "GetSession !s", "Logged on to Exchange."
    End If

End Sub




Private Sub OLDGetSession()
    Dim strPublicRootID As String
    Dim tagPublicRootEntryID As Long
    'Dim strProfile As String

    Err.Clear
    
    On Error Resume Next
    
    tagPublicRootEntryID = &H66310102
    
    'Create new session
    Set mpsSession = New MAPI.Session
    
    'dersom det er angitt et profilnavn i ini fila, bruk den, ellers logg p� den gjeldende session
    If strProfileName = "" Then
        'logg p� med gjeldende profil
        mpsSession.Logon , , False, False, 0, True
    Else
        'logg p� angitt profil
        mpsSession.Logon ProfileName:=strProfileName, ShowDialog:=False, NewSession:=True, ParentWindow:=0, NoMail:=True
    End If
    
    'Get InfoStore and root folder
    Set InfoStore = Nothing
    Set fRoot = Nothing
      
    
    'Language independent public folder fetch
    For Each InfoStore In mpsSession.InfoStores
       strPublicRootID = InfoStore.Fields(tagPublicRootEntryID).Value()
       ' Get root folder
       fRoot = mpsSession.GetFolder(strPublicRootID, InfoStore.ID)
       Exit For
    Next
   
    
    'Logger.Log LOGFILE, "GetSession !s", "Getting InfoStore and RootFolder"
    'If strInfoStoreName = "" Then
    '    Set isInfoStore = mpsSession.InfoStores(2)
    '    strPublicRootID = isInfoStore.Fields(tagPublicRootEntryID).Value
    '    Set fRoot = mpsSession.GetFolder(strPublicRootID, isInfoStore.ID)
    'Else
    '    Set isInfoStore = GetInfoStoreByName(mpsSession, strInfoStoreName)
    '    strPublicRootID = isInfoStore.Fields(tagPublicRootEntryID).Value
    '    Set fRoot = mpsSession.GetFolder(strPublicRootID, isInfoStore.ID)
    'End If
            
    'Check errors
    If Err.Number <> 0 Then
        Logger.Log LOGFILE, "GetSession !x", Err.Number & " " & Err.Description
        mpsSession.Logoff
        Set mpsSession = Nothing
        Sleep nRetryInterval
    Else
        Logger.Log LOGFILE, "GetSession !s", "Logged on to Exchange."
    End If
        
End Sub

Private Function GetInfoStoreByName(ses As MAPI.Session, strInfoStoreName As String) As InfoStore

    Dim infs As InfoStores
    Dim inf As InfoStore
    
    Set infs = ses.InfoStores
    
    For Each inf In infs
        If inf.Name = strInfoStoreName Then
            Set GetInfoStoreByName = inf
            Exit Function
        End If
    Next inf
    
    'if program reaches this point the infostore was not found
    Set GetInfoStoreByName = Nothing
    Err.Raise vbObjectError + 13, "cParseFile.GetInfoStoreByName", "InfoStore not found: " & strInfoStoreName & ". Check ini file."
    Logger.Log LOGFILE, "GetInfoStoreByName !x", "** InfoStore not found: " & strInfoStoreName & " **"
    End
    
End Function

Private Function GetFolderByName(ByVal strFolderName As String, colFolders As Folders) As Folder

    Dim F As Folder                     'For each folder...
    Dim TempF As Folder                 'Intermediate foldername
    Dim strRemFolderName As String      'Remaining part of folder path
    Dim strSingleFolderName As String   'Single foldername
    Dim i As Integer
    
    'strRemFolderName = strRootFolder & "\" & strFolderName
    strRemFolderName = strFolderName
    
    Do
        i = InStr(1, strRemFolderName, "\")
        If i > 0 Then
            strSingleFolderName = Left(strRemFolderName, i - 1)
        Else
            strSingleFolderName = strRemFolderName
        End If
        
        'Loop through folders
        For Each F In colFolders
            
            If LCase(F.Name) = LCase(strSingleFolderName) Then
                
                'Set current folder to F
                Set TempF = F
                
                'Limiting folders collection to subfolders of current folder
                Set colFolders = TempF.Folders
                Exit For
                
            End If
            
        Next F
        
        If i > 0 Then
            strRemFolderName = Right(strRemFolderName, Len(strRemFolderName) - i)
        Else
            Set GetFolderByName = TempF
            Set TempF = Nothing
            Exit Function
        End If
        
    Loop Until i = 0
    
    'If program reaches here, folder was not found: return Nothing
    MsgBox "Folder not found.", "Error"
    Set GetFolderByName = Nothing
    Err.Raise vbObjectError + 12, "Form.GetFolderByName", "Folder not found: " & strRemFolderName & "."
    Logger.Log LOGFILE, "GetFolderByName !x", "** Folder not found: " & strRemFolderName & " **"
End Function


